using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure.EventGrid.Models;
using System;
using System.Text.RegularExpressions;

namespace AzPreProcessors
{
    public static class SqlDataLoaderFn
    {
        [FunctionName("SqlDataLoaderFn")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {

            log.Info($"C# HTTP trigger function begun");
            string response = string.Empty;
            const string SubscriptionValidationEvent = "Microsoft.EventGrid.SubscriptionValidationEvent";
            const string StorageBlobCreatedEvent = "Microsoft.Storage.BlobCreated";

            string requestContent = await req.Content.ReadAsStringAsync();
            EventGridEvent[] eventGridEvents = JsonConvert.DeserializeObject<EventGridEvent[]>(requestContent);

            foreach (EventGridEvent eventGridEvent in eventGridEvents)
            {
                JObject dataObject = eventGridEvent.Data as JObject;

                // Deserialize the event data into the appropriate type based on event type 
                if (string.Equals(eventGridEvent.EventType, SubscriptionValidationEvent, StringComparison.OrdinalIgnoreCase))
                {
                    var eventData = dataObject.ToObject<SubscriptionValidationEventData>();
                    log.Info($"Got SubscriptionValidation event data, validation code: {eventData.ValidationCode}, topic: {eventGridEvent.Topic}");

                    // Do any additional validation (as required) and then return back the below response
                    var responseData = new SubscriptionValidationResponse();
                    responseData.ValidationResponse = eventData.ValidationCode;
                    return req.CreateResponse(HttpStatusCode.OK, responseData);
                }

                else if (string.Equals(eventGridEvent.EventType, StorageBlobCreatedEvent, StringComparison.OrdinalIgnoreCase))
                {
                    var eventData = dataObject.ToObject<StorageBlobCreatedEventData>();
                    log.Info($"Got BlobCreated event data, blob URI {eventData.Url}");
                    if (eventData.Url.EndsWith("csv")&& !eventData.Url.Contains("_gsdata_"))
                    {

                        await loadBlob(eventData.Url, log);

                    }
                }
            }

            return req.CreateResponse(HttpStatusCode.OK, response);

        }

        private async static Task loadBlob(string name, TraceWriter log)
        {

            name = System.IO.Path.GetFileName(name);

            log.Info($"the fiilename is {name}");
            string connectionString = "DefaultEndpointsProtocol=https;AccountName=limekilnstor;AccountKey=Xl9hMnnVWyGtm1SuQNc9UMMAlRmahiBereXwctUQDO+ZaOnHkbbazoB6HnyTl2T8I8ZHRCLpQ5BfkTCKESWVyw==";

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient client = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = client.GetContainerReference("input");

            CloudBlockBlob blob = container.GetBlockBlobReference(name);

            using (Stream memoryStream = new MemoryStream())
            {

                await blob.DownloadToStreamAsync(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);

                await CSVLoader.DataTableLoader(name,memoryStream, log);
                /*                using (StreamReader streamReader = new StreamReader(memoryStream))
                                {
                                    Regex regex = new Regex("Deal");
                                    int lineCount = 0;
                                    while (!streamReader.EndOfStream)
                                    {
                                        ++lineCount;
                                        string textLine = await streamReader.ReadLineAsync();

                                        if (textLine.Length >0)
                                        {
                                            if (regex.Match(textLine).Success)
                                            {
                                                log.Info($"Match: \"{textLine}\" -- line: {lineCount}");
                                            }
                                        }
                                    }
                                    */
            }
        }

    }
}
