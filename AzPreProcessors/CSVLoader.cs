﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CsvHelper;
using System.Data;
using Microsoft.Azure.WebJobs.Host;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace AzPreProcessors
{
    class CSVLoader
    {
        public async static Task DataTableLoader(string name,Stream stream, TraceWriter log)
        {
            DataTable personList = new DataTable();
            using (var csv = new CsvReader(new StreamReader(stream)))
            {
                csv.Read();
                csv.ReadHeader();
                csv.Configuration.HasHeaderRecord = true;
                foreach (var header in csv.Context.HeaderRecord)
                {
                    if (!personList.Columns.Contains(header))
                        personList.Columns.Add(header);
                }

                while (csv.Read())
                {
                    var row = personList.NewRow();
                    foreach (DataColumn col in personList.Columns)
                    {
                        row[col.ColumnName] = csv.GetField(col.DataType, col.ColumnName);
                    }
                    personList.Rows.Add(row);
                }
            }


            log.Info($"There are {personList.Columns.Count} columns and {personList.Rows.Count} parsed.");

            name = name.Substring(13);
            name = name.Replace(".csv", string.Empty);
            name = name.Replace(" ", string.Empty);
            name = name.Replace("-", string.Empty);
            string createTbl = createTableString(name, personList);

            string conStr = "Server = tcp:domanic.database.windows.net,1433; Initial Catalog = limeDB; Persist Security Info = False; User ID = dchoi; Password =Volume11; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                await con.OpenAsync();
                log.Info($"{createTbl}");
                using (SqlCommand cmd = new SqlCommand("drop table if exists " + name, con)){
                    await cmd.ExecuteNonQueryAsync();
                }

                using (SqlCommand cmd = new SqlCommand(createTbl,con))
                {
                    var rows = await cmd.ExecuteNonQueryAsync();
                    log.Info($"{rows} rows were processed");
                }
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    bulkCopy.BatchSize = 4000;
                    bulkCopy.BulkCopyTimeout = 0;
                    bulkCopy.DestinationTableName = "dbo."+name;

                    try
                    {
                        await bulkCopy.WriteToServerAsync(personList);
                    } catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }
            }
        }

        public static string createTableString(string tableName,DataTable table)
        {
            string sqlsc;
            sqlsc = "  CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Int32":
                        sqlsc += " int ";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint ";
                        break;
                    case "System.Int16":
                        sqlsc += " smallint";
                        break;
                    case "System.Byte":
                        sqlsc += " tinyint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal ";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime ";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" nvarchar({0}) ", table.Columns[i].MaxLength == -1 ? "max" : table.Columns[i].MaxLength.ToString());
                        break;
                }
                if (table.Columns[i].AutoIncrement)
                    sqlsc += " IDENTITY(" + table.Columns[i].AutoIncrementSeed.ToString() + "," + table.Columns[i].AutoIncrementStep.ToString() + ") ";
                if (!table.Columns[i].AllowDBNull)
                    sqlsc += " NOT NULL ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + "\n)";
        }
    }
}
